#!/bin/bash

set -x
set -e

SHARED_DIR=/shared

# first, eventually clone the fdo-bots repository
FDO_BOTS=${SHARED_DIR}/fdo-bots
if [ ! -d ${FDO_BOTS} ]
then
  git clone git@gitlab.freedesktop.org:freedesktop/fdo-bots.git $FDO_BOTS
  pushd $FDO_BOTS
  git submodule init
  popd
fi

# update fdo-bots
pushd $FDO_BOTS
git remote update
git reset --hard origin/main
git submodule update
popd

# clone damspam if required
DAMSPAM=${SHARED_DIR}/damspam
if [ ! -d ${DAMSPAM} ]
then
  git clone https://gitlab.freedesktop.org/freedesktop/damspam.git $DAMSPAM
fi

# create venv for damspam if required
DAMSPAM_VENV=${SHARED_DIR}/venv/damspam
if [ ! -d ${DAMSPAM_VENV} ]
then
  python3 -m venv ${DAMSPAM_VENV}
fi

# deploy damspam in a venv
pushd ${DAMSPAM}
source ${DAMSPAM_VENV}/bin/activate
git remote update
git reset --hard origin/main
pip install .
deactivate
popd

# create venv for jinja2 if required
JINJA2_VENV=${SHARED_DIR}/venv/jinja2
if [ ! -d ${JINJA2_VENV} ]
then
  python3 -m venv ${JINJA2_VENV}
  source ${JINJA2_VENV}/bin/activate
  pip install jinja2 jinja-cli
  deactivate
fi

# re-render our Settings.yaml
source ${JINJA2_VENV}/bin/activate
jinja -d ${FDO_BOTS}/helm-gitlab-secrets/damspam.yaml -o ${SHARED_DIR}/Settings.yaml ${FDO_BOTS}/values/damspam/Settings.tmpl
deactivate
